/*
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 *
 */

/**
 * This class contains information about any Media errors.
 */
/*
 According to :: http://dev.w3.org/html5/spec-author-view/video.html#mediaerror
 We should never be creating these objects, we should just implement the interface
 which has 1 property for an instance, 'code'

 instead of doing :
 errorCallbackFunction( new MediaError(3,'msg') );
 we should simply use a literal :
 errorCallbackFunction( {'code':3} );
 */

(function () {
    "use strict";

    if (!window.MediaError) {
        window.MediaError = window.MediaError = function (code, msg) {
            this.code = (code !== undefined) ? code : null;
            this.message = msg || ""; // message is NON-standard! do not use!
        };
    }

    try {
        window.MediaError.MEDIA_ERR_NONE_ACTIVE = window.MediaError.MEDIA_ERR_NONE_ACTIVE || 0;
    } catch (e) {
    }
    try {
        window.MediaError.MEDIA_ERR_ABORTED = window.MediaError.MEDIA_ERR_ABORTED || 1;
    } catch (e) {
    }
    try {
        window.MediaError.MEDIA_ERR_NETWORK = window.MediaError.MEDIA_ERR_NETWORK || 2;
    } catch (e) {
    }
    try {
        window.MediaError.MEDIA_ERR_DECODE = window.MediaError.MEDIA_ERR_DECODE || 3;
    } catch (e) {
    }
    try {
        window.MediaError.MEDIA_ERR_NONE_SUPPORTED = window.MediaError.MEDIA_ERR_NONE_SUPPORTED || 4;
    } catch (e) {
    }
    try {
        window.MediaError.MEDIA_ERR_SRC_NOT_SUPPORTED = window.MediaError.MEDIA_ERR_SRC_NOT_SUPPORTED || 4;
    } catch (e) {
    }

    window.cordova.addConstructor(window.MediaError);
}());

